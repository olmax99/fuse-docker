# just in case ...
findmnt -o TARGET,PROPAGATION /mnt
sudo chown -R ${UID}:${UID} ${DATA_DIR}
ls -al ${DATA_DIR} 

# run with given user
exec sudo -u ${UID} gcsfuse --foreground --key-file=${GOOGLE_APPLICATION_CREDENTIALS} ${BUCKET_NAME} ${DATA_DIR}
